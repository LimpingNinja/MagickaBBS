#include "../../deps/jamlib/jam.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h>
#include <limits.h>

#define TRUE 1
#define FALSE 0


int jam_packmb(const char *filename) {
	s_JamBase *NewBase_PS;
	s_JamBase *Base_PS;
	s_JamBaseHeader BaseHeader_S;
	s_JamMsgHeader Header_S;
	s_JamSubPacket *SubPacket_PS;

	char buf[PATH_MAX];
	char oldname[PATH_MAX];
	char tmpname[PATH_MAX];
	int del;
	int num;
	int firstwritten;
	int res, res1, res2;
	char *msgtext;
	int total, basenum;

	if (JAM_OpenMB((char *)filename, &Base_PS)) {
		printf(" Failed to open messagebase \"%s\"\n", filename);
		return TRUE;
	}

	if (JAM_LockMB(Base_PS, 10)) {
		printf(" Timeout when trying to lock messagebase \"%s\"\n", filename);
		JAM_CloseMB(Base_PS);
		return TRUE;
	}

	if(JAM_GetMBSize(Base_PS,&total))
	{
		printf(" Failed to get size of messagebase \"%s\"\n",filename);
		JAM_UnlockMB(Base_PS);
		JAM_CloseMB(Base_PS);
		return(TRUE);
	}

	if(JAM_ReadMBHeader(Base_PS,&BaseHeader_S))
	{
		printf(" Failed to read header of messagebase \"%s\"\n",filename);
		JAM_UnlockMB(Base_PS);
		JAM_CloseMB(Base_PS);
		return(TRUE);
	}


	snprintf(buf, sizeof buf, "%s.jmtemp", filename);

	if(JAM_CreateMB(buf,1,&NewBase_PS))
   	{
      	printf(" Failed to create new messagebase \"%s\"\n",buf);
		JAM_UnlockMB(Base_PS);
		JAM_CloseMB(Base_PS);
		return(TRUE);
	}

	if(JAM_LockMB(NewBase_PS,10))
	{
		printf(" Timeout when trying to lock messagebase \"%s\"\n",buf);
		JAM_UnlockMB(Base_PS);
		JAM_CloseMB(Base_PS);
		JAM_CloseMB(NewBase_PS);
		JAM_RemoveMB(NewBase_PS,buf);
		return(TRUE);
	}



	/* Copy messages */

    del=0;
    num=0;
	firstwritten=FALSE;

	basenum = BaseHeader_S.BaseMsgNum;

	BaseHeader_S.ActiveMsgs = 0;

	while(num < total) {
		res=JAM_ReadMsgHeader(Base_PS,num,&Header_S,NULL);

        if(res)
		{
			if(res == JAM_NO_MESSAGE)
			{
				if(firstwritten) 
				{
					JAM_AddEmptyMessage(NewBase_PS);
				}
				else 
				{
					BaseHeader_S.BaseMsgNum++;
				}
				del++;				
				
			}
			else
			{
		    	printf(" Failed to read message %d, cannot pack messagebase\n",num+basenum);
				JAM_UnlockMB(Base_PS);
				JAM_CloseMB(Base_PS);
				JAM_UnlockMB(NewBase_PS);
				JAM_CloseMB(NewBase_PS);
				JAM_RemoveMB(NewBase_PS,buf);
				return(TRUE);
			}
		}
		else
		{
			if(Header_S.Attribute & JAM_MSG_DELETED)
			{
				if(firstwritten) 
				{
					JAM_AddEmptyMessage(NewBase_PS);
				}
				else
				{
					BaseHeader_S.BaseMsgNum++;
				}
				del++;
			}
			else
			{
            	if(!firstwritten)
            	{
					/* Set basenum */

					res=JAM_WriteMBHeader(NewBase_PS,&BaseHeader_S);

					if(res)
					{
						printf(" Failed to write messagebase header, cannot pack messagebase\n");
						JAM_UnlockMB(Base_PS);
						JAM_CloseMB(Base_PS);
						JAM_UnlockMB(NewBase_PS);
						JAM_CloseMB(NewBase_PS);
						JAM_RemoveMB(NewBase_PS,buf);
						return(TRUE);
					}

					firstwritten=TRUE;
				}

				/* Read header with all subpackets*/

				res=JAM_ReadMsgHeader(Base_PS,num,&Header_S,&SubPacket_PS);

				if(res)
				{
			    	printf(" Failed to read message %d, cannot pack messagebase\n",num+basenum);
					JAM_UnlockMB(Base_PS);
					JAM_CloseMB(Base_PS);
					JAM_UnlockMB(NewBase_PS);
					JAM_CloseMB(NewBase_PS);
					JAM_RemoveMB(NewBase_PS,buf);
					return(TRUE);
				}

				/* Read message text */

				msgtext=NULL;

				if(Header_S.TxtLen)
				{
					if(!(msgtext=malloc(Header_S.TxtLen)))
					{
						printf("Out of memory\n");
						JAM_DelSubPacket(SubPacket_PS);
						JAM_UnlockMB(Base_PS);
						JAM_CloseMB(Base_PS);
						JAM_UnlockMB(NewBase_PS);
						JAM_CloseMB(NewBase_PS);
						JAM_RemoveMB(NewBase_PS,buf);
						return(TRUE);
					}			

					res=JAM_ReadMsgText(Base_PS,Header_S.TxtOffset,Header_S.TxtLen,msgtext);

					if(res)
					{
						printf(" Failed to read message %d, cannot pack messagebase\n",num+basenum);
						JAM_DelSubPacket(SubPacket_PS);
						JAM_UnlockMB(Base_PS);
						JAM_CloseMB(Base_PS);
						JAM_UnlockMB(NewBase_PS);
						JAM_CloseMB(NewBase_PS);
						JAM_RemoveMB(NewBase_PS,buf);
						return(TRUE);
					}
				}

				/* Write new message */

				res=JAM_AddMessage(NewBase_PS,&Header_S,SubPacket_PS,msgtext,Header_S.TxtLen);

				if(msgtext) free(msgtext);
				JAM_DelSubPacket(SubPacket_PS);

				BaseHeader_S.ActiveMsgs++;

				if(res)
				{
					printf(" Failed to copy message %d (disk full?), cannot pack messagebase\n",num+basenum);
					JAM_UnlockMB(Base_PS);
					JAM_CloseMB(Base_PS);
					JAM_UnlockMB(NewBase_PS);
					JAM_CloseMB(NewBase_PS);
					JAM_RemoveMB(NewBase_PS,buf);
					return(TRUE);
				}
			}
		}
			
		num++;
	}

	/* Write back header */

	BaseHeader_S.ModCounter++;

	res=JAM_WriteMBHeader(NewBase_PS,&BaseHeader_S);

	if(res)
	{
		printf(" Failed to write messagebase header, cannot pack messagebase\n");
		JAM_UnlockMB(Base_PS);
		JAM_CloseMB(Base_PS);
		JAM_UnlockMB(NewBase_PS);
		JAM_CloseMB(NewBase_PS);
        JAM_RemoveMB(NewBase_PS,buf);
        return(TRUE);
    }

	JAM_UnlockMB(Base_PS);
	JAM_CloseMB(Base_PS);

	JAM_UnlockMB(NewBase_PS);
	JAM_CloseMB(NewBase_PS);

	/* This could not be done with JAMLIB... */

	snprintf(oldname, sizeof oldname, "%s%s", filename, EXT_HDRFILE);
	snprintf(tmpname, sizeof tmpname, "%s.jmtemp%s", filename, EXT_HDRFILE);
	res1 = unlink(oldname);
	res2 = rename(tmpname,oldname);

	if(res1 == 0 && res2 == 0)
	{
		snprintf(oldname, sizeof oldname, "%s%s", filename, EXT_TXTFILE);
		snprintf(tmpname, sizeof tmpname, "%s.jmtemp%s", filename, EXT_TXTFILE);
		res1 = unlink(oldname);
		res2 = rename(tmpname,oldname);
	}
		
	if(res1 == 0 && res2 == 0)
	{
		snprintf(oldname, sizeof oldname, "%s%s", filename, EXT_IDXFILE);
		snprintf(tmpname, sizeof tmpname, "%s.jmtemp%s", filename, EXT_IDXFILE);
		res1 = unlink(oldname);
		res2 = rename(tmpname,oldname);
	}
		
	if(res1 == 0 && res2 == 0)
	{
		snprintf(tmpname, sizeof tmpname, "%s.jmtemp%s", filename, EXT_LRDFILE);
		res2 = unlink(tmpname);
	}
		
	if(res1 != 0 || res2 != 0)
	{
		printf(" Failed to update area. The area might be in use by another program.\n");
		return(TRUE);
	}

	printf(" %d deleted messages removed from messagebase\n",del);

	return FALSE;
}


int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s [jam_base_path]\r\n", argv[0]);
        return TRUE;
    }

    return jam_packmb(argv[1]);

}