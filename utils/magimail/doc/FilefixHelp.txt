
            Description of MagiMail's built-in FileFix
            ==========================================

What is FileFix?
----------------
FileFix is a feature present at most FidoNet nodes, either built-in in
the tosser or as a stand-alone program. FileFix allows you to connect
and disconnect to file areas, change your password and change some
other things in your configuration without asking your feed/boss to do
it for you.

How do I talk to FileFix?
-------------------------
You communicate with FileFix using netmail messages. The message header
of a message to FileFix should look like this:

From: Johan Billing
  To: FileFix
Subj: <password> [<switches>]

<FileFix commands>
...
---

<Password> is your private FileFix password that you need to make sure
that nobody else alters your configuration. Your FileFix password is
assigned to you by your boss/feed, so you may have to ask him about a 
password.

[<switches>] are extra commands that you can send to FileFix. Currently
there is only one switch:

 -l or -q         Send list of all areas

This switch is only here to be compatible with other FileFix programs.
It is recommended that you use the %-commands described later instead.

"---" marks the end of an FileFix message.

NOTE: The name doesn't necessarily have to be FileFix, your boss may have
configured MagiMail to use other names instead.

What commands can I put in the text?
------------------------------------

Change connected areas:

+areaname               Connect to an area. 

-areaname               Disconnect from an area

Special commands

%HELP                   Send this text

%LIST                   Send list of all areas
%QUERY                  Send list of all linked areas
%UNLINKED               Send list of all unlinked areas


Examples:
=========

+R20_TRASHCAN           Connect to R20_TRASHCAN

%LIST                   Send list of areas

