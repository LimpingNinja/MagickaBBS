#include <stdlib.h>
#include <string.h>
#include "node.h"

struct node4d *parse4d(const char *str) {
	if (str == NULL) {
		return NULL;
	}
	struct node4d *ret = (struct node4d *)malloc(sizeof(struct node4d));
	int c;
	int state = 0;

	ret->zone = 0;
	ret->net = 0;
	ret->node = 0;
	ret->point = 0;

	for (c = 0; c < strlen(str); c++) {
		switch (str[c]) {
			case ':':
				state = 1;
				break;
			case '/':
				state = 2;
				break;
			case '.':
				state = 3;
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9': {
				switch (state) {
					case 0:
						ret->zone = ret->zone * 10 + (str[c] - '0');
						break;
					case 1:
						ret->net = ret->net * 10 + (str[c] - '0');
						break;
					case 2:
						ret->node = ret->node * 10 + (str[c] - '0');
						break;
					case 3:
						ret->point = ret->point * 10 + (str[c] - '0');
						break;
				}
			} break;
			default:
				free(ret);
				return NULL;
		}
	}
	return ret;
}

int node4deq(struct node4d *n1, struct node4d *n2) {
	if (n1->zone == n2->zone && n1->net == n2->net && n1->node == n2->node && n1->point == n2->point) {
		return 1;
	}

	return 0;
}