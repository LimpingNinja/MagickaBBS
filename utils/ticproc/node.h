#ifndef __NODE_H__
#define __NODE_H__

struct node4d {
	unsigned short zone;
	unsigned short net;
	unsigned short node;
	unsigned short point;
};

struct node4d *parse4d(const char *str);
int node4deq(struct node4d *n1, struct node4d *n2);

#endif
