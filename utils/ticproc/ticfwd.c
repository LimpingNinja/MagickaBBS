#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>
#include <libgen.h>
#include "../../src/inih/ini.h"
#include "crc32.h"
#include "node.h"

struct node_t {
	struct node4d *aka;
	char *password;
	char *outbox;
};

struct area_t {
	char *name;
	struct node4d **nodes;
	int node_count;
};

char *tmpfolder;

struct area_t **areas;
int area_count = 0;

struct node_t **nodes;
int node_count = 0;

static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
	if (strcasecmp(section, "main") == 0) {
		if (strcasecmp(name, "temp folder") == 0) {
			tmpfolder = strdup(value);
		}
	} else if (strcasecmp(section, "areas") == 0) {
		if (area_count == 0) {
			areas = (struct area_t **)malloc(sizeof(struct area_t *));
		} else {
			areas = (struct area_t **)realloc(areas, sizeof(struct area_t *) * (area_count + 1));
		}

		areas[area_count] = (struct area_t *)malloc(sizeof(struct area_t));
		areas[area_count]->name = strdup(name);
		areas[area_count]->node_count = 0;
		
		FILE *fptr;
		char buffer[256];

		fptr = fopen(value, "r");
		if (fptr) {
			fgets(buffer, 256, fptr);
			while (!feof(fptr)) {
				if (buffer[0] != ';' && buffer[0] != '\n') {
					if (areas[area_count]->node_count == 0) {
						areas[area_count]->nodes = (struct node4d **)malloc(sizeof(struct node4d  *));
					} else {
						areas[area_count]->nodes = (struct node4d  **)realloc(areas[area_count]->nodes, sizeof(struct node4d  *) * (areas[area_count]->node_count + 1));
					}

					if (strlen(buffer) > 1) {
						if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
						if (buffer[strlen(buffer) - 1] == '\r') buffer[strlen(buffer) - 1] = '\0';
					}
					areas[area_count]->nodes[areas[area_count]->node_count] = parse4d(buffer);
					if (areas[area_count]->nodes[areas[area_count]->node_count] == NULL) {
						fprintf(stderr, "Invalid Node %s\n", buffer);
						fclose(fptr);
						return 0;
					}
					areas[area_count]->node_count++;
				}
				fgets(buffer, 256, fptr);
			}
			fclose(fptr);
		}

		area_count++;
	} else {
		struct node4d *n = parse4d(section);
		if (!n) {
			fprintf(stderr, "Invalid Node %s\n", section);
		}
		for (int i = 0; i < node_count; i++) {

			if (node4deq(nodes[i]->aka, n)) {
				if (strcasecmp(name, "password") == 0) {
					nodes[i]->password = strdup(value);
				} else if (strcasecmp(name, "outbox") == 0) {
					nodes[i]->outbox = strdup(value);
				}
				free(n);
				return 1;
			}
		}

		if (node_count == 0) {
			nodes = (struct node_t **)malloc(sizeof(struct node_t *));
		} else {
			nodes = (struct node_t **)realloc(nodes, sizeof(struct node_t *) * (node_count + 1));
		}

		nodes[node_count] = malloc(sizeof(struct node_t));

		nodes[node_count]->aka = n;

		if (strcasecmp(name, "password") == 0) {
			nodes[node_count]->password = strdup(value);
		} else if (strcasecmp(name, "outbox") == 0) {
			nodes[node_count]->outbox = strdup(value);
		}
		node_count++;
	}

	return 1;
}

int copy_file(char *src, char *dest) {
	FILE *src_file;
	FILE *dest_file;

	char c;

	src_file = fopen(src, "rb");
	if (!src_file) {
		return -1;
	}
	dest_file = fopen(dest, "wb");
	if (!dest_file) {
		fclose(src_file);
		return -1;
	}

	while(1) {
		c = fgetc(src_file);
		if (!feof(src_file)) {
			fputc(c, dest_file);
		} else {
			break;
		}
	}

	fclose(src_file);
	fclose(dest_file);
	return 0;
}


int main(int argc, char **argv) {
	int areano = -1;
	char tmptic[PATH_MAX];
	char tmpfile[PATH_MAX];
	char buffer[256];
	time_t thetime = time(NULL);
	struct tm ltime;
	char *days[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
	char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	gmtime_r(&thetime, &ltime);

	struct node4d *myaka;

	char **tic_lines;
	int tic_line_count = 0;

	if (argc < 6) {
		fprintf(stderr, "Usage:\n%s tichatch.ini FILE_SUB ticfile filename myaka\n", argv[0]);
		exit(-1);
	}

	if (ini_parse(argv[1], handler, NULL) <0) {
		fprintf(stderr, "Unable to load configuration ini (%s)!\n", argv[1]);
		exit(-1);
	}

	myaka = parse4d(argv[5]);
	if (!myaka) {
		fprintf(stderr, "Invalid my AKA\n");
		exit(-1);
	}

	for (int i = 0; i< area_count;i++) {
		if (strcasecmp(areas[i]->name, argv[2]) == 0) {
			areano = i;
			break;
		}
	}


	if (areano == -1) {
		fprintf(stderr, "Area %s not defined!\n", argv[2]);
		exit(-1);
	}


	FILE *fptr_tic = fopen(argv[3], "r");
	if (fptr_tic) {
		fgets(buffer, 256, fptr_tic);
		while (!feof(fptr_tic)) {
			if (tic_line_count == 0) {
				tic_lines = (char **)malloc(sizeof(char *));
			} else {
				tic_lines = (char **)realloc(tic_lines, sizeof(char *) * (tic_line_count + 1));
			}
			if (strlen(buffer) > 1) {
				if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
			}
			if (strlen(buffer) > 1) {
				if (buffer[strlen(buffer) - 1] == '\r') buffer[strlen(buffer) - 1] = '\0';
			}
			tic_lines[tic_line_count] = strdup(buffer);
			tic_line_count++;
			fgets(buffer, 256, fptr_tic);
		}
	} else {
		fprintf(stderr, "Unable to open tic file %s\n", argv[3]);
		exit(-1);
	}

	char *ticpathc = strdup(argv[3]);
	char *tname = basename(ticpathc);
	char *filepathc = strdup(argv[4]);
	char *fname = basename(filepathc);

	for (int i=0;i<areas[areano]->node_count;i++) {
		int nodeno = -1;
		int sentalready = 0;

		for (int j = 0; j < node_count; j++) {
			if (node4deq(nodes[j]->aka, areas[areano]->nodes[i])) {
				nodeno = j;
				break;
			}
		}

		if (nodeno == -1) {
			fprintf(stderr, "Invalid node %s subscribed to %s\n", areas[areano]->nodes[i], areas[areano]->name);
			continue;
		}

		struct node4d *tmpn;

		for (int j=0;j<tic_line_count;j++) {
			if (strncasecmp(tic_lines[j], "Seenby ", 7) == 0) {
				tmpn = parse4d(&tic_lines[j][7]);
				if (tmpn != NULL) {
					if (node4deq(tmpn, nodes[nodeno]->aka)) {
						free(tmpn);
						sentalready = 1;
						break;
					} else {
						free(tmpn);
					}
				}
			}
		}
		if (sentalready) {
			continue;
		}
		// create tic file

		snprintf(tmptic, sizeof tmptic, "%s/%s", tmpfolder, tname);

		FILE *fptr = fopen(tmptic, "wb");

		if (fptr) {

			for (int j=0;j<tic_line_count;j++) {
				if (strncasecmp(tic_lines[j], "Pw ", 3) == 0) {
					fprintf(fptr, "Pw %s\r\n", nodes[nodeno]->password);
				} else if (strncasecmp(tic_lines[j], "From ", 3) == 0) {
					fprintf(fptr, "From %d:%d/%d.%d\r\n", myaka->zone, myaka->net, myaka->node, myaka->point);
				} else if (strncasecmp(tic_lines[j], "Seenby ", 7) != 0 && strncasecmp(tic_lines[j], "Path ", 5) != 0) {
					fprintf(fptr, "%s\r\n", tic_lines[j]);
				}
			}


			for (int j=0;j<tic_line_count;j++) {
				// print paths
				if (strncasecmp(tic_lines[j], "Path ", 5) == 0) {
					fprintf(fptr, "%s\r\n", tic_lines[j]);
				}

			}


			fprintf(fptr, "Path %d:%d/%d.%d %u %s %s %02d:%02d:%02d %d UTC\r\n", myaka->zone, myaka->net, myaka->node, myaka->point, thetime, days[ltime.tm_wday], months[ltime.tm_mon], ltime.tm_hour, ltime.tm_min, ltime.tm_sec, ltime.tm_year + 1900);


			for (int j=0;j<tic_line_count;j++) {
				// print old seenbys
				if (strncasecmp(tic_lines[j], "Seenby ", 7) == 0) {
					fprintf(fptr, "%s\r\n", tic_lines[j]);
				}
			}

			for (int j=0;j<areas[areano]->node_count;j++) {
				int node_found = 0;
				for (int x = 0; x < tic_line_count; x++) {
					if (strncasecmp(tic_lines[x], "Seenby ", 7) == 0) {
						snprintf(buffer, sizeof buffer, "%d:%d/%d.%d",  areas[areano]->nodes[j]->zone,  areas[areano]->nodes[j]->net,  areas[areano]->nodes[j]->node,  areas[areano]->nodes[j]->point);

						if (strcmp(&tic_lines[x][7], buffer) == 0) {
							node_found = 1;
							break;
						}
					}
				}
				if (!node_found) {
					fprintf(fptr, "Seenby %d:%d/%d.%d\r\n", areas[areano]->nodes[j]->zone,  areas[areano]->nodes[j]->net,  areas[areano]->nodes[j]->node,  areas[areano]->nodes[j]->point);
				}
			}

			fclose(fptr);
			// copy tic file to outbox
			snprintf(tmpfile, sizeof tmpfile, "%s/%s", nodes[nodeno]->outbox, tname);
			copy_file(tmptic, tmpfile);

			// copy file to outbox
			snprintf(tmpfile, sizeof tmpfile, "%s/%s", nodes[nodeno]->outbox, fname);
			copy_file(argv[3], tmpfile);

			unlink(tmptic);

		}
	}
	free(filepathc);
	free(ticpathc);
}