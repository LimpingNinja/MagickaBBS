#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/stat.h>
#include "../../src/inih/ini.h"
#include "crc32.h"
#include "node.h"

struct node_t {
	struct node4d *aka;
	char *password;
	char *outbox;
};

struct area_t {
	char *name;
	struct node4d **nodes;
	int node_count;
};

struct node4d *myaka;
char *tmpfolder;

struct area_t **areas;
int area_count = 0;

struct node_t **nodes;
int node_count = 0;

static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
	if (strcasecmp(section, "main") == 0) {
		if (strcasecmp(name, "aka") == 0) {
			myaka = parse4d(value);
			if (!myaka) {
				fprintf(stderr, "Invalid Node %s\n", value);
				return 0;
			}
		} else if (strcasecmp(name, "temp folder") == 0) {
			tmpfolder = strdup(value);
		}
	} else if (strcasecmp(section, "areas") == 0) {
		if (area_count == 0) {
			areas = (struct area_t **)malloc(sizeof(struct area_t *));
		} else {
			areas = (struct area_t **)realloc(areas, sizeof(struct area_t *) * (area_count + 1));
		}

		areas[area_count] = (struct area_t *)malloc(sizeof(struct area_t));
		areas[area_count]->name = strdup(name);
		areas[area_count]->node_count = 0;
		
		FILE *fptr;
		char buffer[256];

		fptr = fopen(value, "r");
		if (fptr) {
			fgets(buffer, 256, fptr);
			while (!feof(fptr)) {
				if (buffer[0] != ';' && buffer[0] != '\n') {
					if (areas[area_count]->node_count == 0) {
						areas[area_count]->nodes = (struct node4d **)malloc(sizeof(struct node4d *));
					} else {
						areas[area_count]->nodes = (struct node4d **)realloc(areas[area_count]->nodes, sizeof(struct node4d *) * (areas[area_count]->node_count + 1));
					}

					if (strlen(buffer) > 1) {
						if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
					}
					if (strlen(buffer) > 1) {
						if (buffer[strlen(buffer) - 1] == '\r') buffer[strlen(buffer) - 1] = '\0';
					}
					areas[area_count]->nodes[areas[area_count]->node_count] = parse4d(buffer);
					if (areas[area_count]->nodes[areas[area_count]->node_count] == NULL) {
						fprintf(stderr, "Invalid Node %s\n", buffer);
						fclose(fptr);
						return 0;
					}
					areas[area_count]->node_count++;
				}
				fgets(buffer, 256, fptr);
			}
			fclose(fptr);
		}

		area_count++;
	} else {
		struct node4d *n = parse4d(section);
		if (!n) {
			fprintf(stderr, "Invalid Node %s\n", section);
			return 0;
		}		
		for (int i = 0; i < node_count; i++) {
			if (node4deq(nodes[i]->aka, n)) {
				if (strcasecmp(name, "password") == 0) {
					nodes[i]->password = strdup(value);
				} else if (strcasecmp(name, "outbox") == 0) {
					nodes[i]->outbox = strdup(value);
				}
				free(n);
				return 1;
			}
		}

		if (node_count == 0) {
			nodes = (struct node_t **)malloc(sizeof(struct node_t *));
		} else {
			nodes = (struct node_t **)realloc(nodes, sizeof(struct node_t *) * (node_count + 1));
		}

		nodes[node_count] = malloc(sizeof(struct node_t));

		nodes[node_count]->aka = n;

		if (strcasecmp(name, "password") == 0) {
			nodes[node_count]->password = strdup(value);
		} else if (strcasecmp(name, "outbox") == 0) {
			nodes[node_count]->outbox = strdup(value);
		}
		node_count++;
	}

	return 1;
}

int copy_file(char *src, char *dest) {
	FILE *src_file;
	FILE *dest_file;

	char c;

	src_file = fopen(src, "rb");
	if (!src_file) {
		return -1;
	}
	dest_file = fopen(dest, "wb");
	if (!dest_file) {
		fclose(src_file);
		return -1;
	}

	while(1) {
		c = fgetc(src_file);
		if (!feof(src_file)) {
			fputc(c, dest_file);
		} else {
			break;
		}
	}

	fclose(src_file);
	fclose(dest_file);
	return 0;
}


int main(int argc, char **argv) {
	int areano = -1;
	char tmptic[PATH_MAX];
	char tmpfile[PATH_MAX];
	char buffer[256];
	time_t thetime = time(NULL);
	struct tm ltime;
	char *days[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
	char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	gmtime_r(&thetime, &ltime);
	struct stat s;

	if (argc < 5) {
		fprintf(stderr, "Usage:\n%s tichatch.ini FILE_SUB filepath descfile [REPLACES]\n", argv[0]);
		exit(-1);
	}


	if (ini_parse(argv[1], handler, NULL) <0) {
		fprintf(stderr, "Unable to load configuration ini (%s)!\n", argv[1]);
		exit(-1);
	}

	for (int i = 0; i< area_count;i++) {
		if (strcasecmp(areas[i]->name, argv[2]) == 0) {
			areano = i;
			break;
		}
	}


	if (areano == -1) {
		fprintf(stderr, "Area %s not defined!\n", argv[2]);
		exit(-1);
	}

	char *filepathc = strdup(argv[3]);
	char *fname = basename(filepathc);
	char *fnamec = strdup(fname);
	char *dot = strrchr(fnamec, '.');

	if (dot != NULL) {
		*dot = '\0';
	}


	if (strlen(fnamec) > 8 || strlen(&dot[1]) > 3) {
		fprintf(stderr, "Attempting to hatch non 8.3 file!\n");
		exit(-1);
	}

	unsigned long crc;
	FILE *fptr_crc = fopen(argv[3], "rb");

	if (!fptr_crc) {
		fprintf(stderr, "Unable to open %s\n", argv[3]);
		exit(-1);		
	}

	if (Crc32_ComputeFile(fptr_crc, &crc) == -1) {
		fprintf(stderr, "Error computing CRC\n");
		fclose(fptr_crc);
		exit(-1);
	}

	fclose(fptr_crc);

	for (int i=0;i<areas[areano]->node_count;i++) {
		int nodeno = -1;
		for (int j = 0; j < node_count; j++) {
			if (node4deq(nodes[j]->aka, areas[areano]->nodes[i])) {
				nodeno = j;
				break;
			}
		}

		if (nodeno == -1) {
			fprintf(stderr, "Invalid node %d:%d/%d.%d subscribed to %s\n", areas[areano]->nodes[i]->zone, areas[areano]->nodes[i]->net, areas[areano]->nodes[i]->node, areas[areano]->nodes[i]->point, areas[areano]->name);
			continue;
		}

		// create tic file

		snprintf(tmptic, sizeof tmptic, "%s/%s.tic", tmpfolder, fnamec);

		FILE *fptr = fopen(tmptic, "wb");

		if (fptr) {
			fprintf(fptr, "Area %s\r\n", areas[areano]->name);
			fprintf(fptr, "Origin %d:%d/%d.%d\r\n", myaka->zone, myaka->net, myaka->node, myaka->point);
			fprintf(fptr, "From %d:%d/%d.%d\r\n", myaka->zone, myaka->net, myaka->node, myaka->point);
			fprintf(fptr, "File %s\r\n", fname);

			FILE *fptr2 = fopen(argv[4], "r");
			if (fptr2) {
				fgets(buffer, 256, fptr2);
				if (strlen(buffer) >= 1) {
					if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
				}
				if (strlen(buffer) >= 1) {
					if (buffer[strlen(buffer) - 1] == '\r') buffer[strlen(buffer) - 1] = '\0';
				}
				fprintf(fptr, "Desc %s\r\n", buffer);
				while (!feof(fptr2)) {
					fprintf(fptr, "Ldesc %s\r\n", buffer);
					fgets(buffer, 256, fptr2);
					if (strlen(buffer) >= 1) {
						if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
					}
					if (strlen(buffer) >= 1) {
						if (buffer[strlen(buffer) - 1] == '\r') buffer[strlen(buffer) - 1] = '\0';
					}
				}
				fclose(fptr2);
			}

			if (argc > 5) {
				fprintf(fptr, "Replaces %s\r\n", argv[5]);
			}

			if (stat(argv[3], &s) == 0) {
				fprintf(fptr, "Size %u\r\n", s.st_size);
			}

			fprintf(fptr, "Pw %s\r\n", nodes[nodeno]->password);
			fprintf(fptr, "Crc %08X\r\n", crc);
			fprintf(fptr, "Path %d:%d/%d.%d %u %s %s %02d:%02d:%02d %d UTC\r\n", myaka->zone, myaka->net, myaka->node, myaka->point, thetime, days[ltime.tm_wday], months[ltime.tm_mon], ltime.tm_hour, ltime.tm_min, ltime.tm_sec, ltime.tm_year + 1900);

			fprintf(fptr, "Seenby %d:%d/%d.%d\r\n", myaka->zone, myaka->net, myaka->node, myaka->point);
			for (int j=0;j<areas[areano]->node_count;j++) {
				if (!node4deq(myaka, areas[areano]->nodes[j])) {
					fprintf(fptr, "Seenby %d:%d/%d.%d\r\n", areas[areano]->nodes[j]->zone, areas[areano]->nodes[j]->net, areas[areano]->nodes[j]->node, areas[areano]->nodes[j]->point);
				}
			}

			fclose(fptr);
			// copy tic file to outbox
			snprintf(tmpfile, sizeof tmpfile, "%s/%s.tic", nodes[nodeno]->outbox, fnamec);
			copy_file(tmptic, tmpfile);

			// copy file to outbox
			snprintf(tmpfile, sizeof tmpfile, "%s/%s", nodes[nodeno]->outbox, fname);
			copy_file(argv[3], tmpfile);

			unlink(tmptic);

		}


	}
	free(filepathc);
	free(fnamec);	
}