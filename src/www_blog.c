#if defined(ENABLE_WWW)

#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "www_tree.h"
#include "bbs.h"
#include "../deps/md4c/md4c-html.h"

extern struct bbs_config conf;

static void www_blog_process_output(const MD_CHAR *text, MD_SIZE size, void *userdata) {
    stralloc_catb((stralloc *)userdata, text, size);
}

char *www_blog_rss(struct MHD_Connection *connection) {
	struct www_tag *page;
	struct www_tag *rss_tag;
	struct www_tag *channel_tag;
	struct www_tag *channel_sub_tag;
	struct www_tag *channel_sub_tag_content;

    int blog_count = blog_get_entry_count();
    
	stralloc str_a;
    int ret;
    
	page = www_tag_new(NULL, "");
	rss_tag = www_tag_new("rss", NULL);
	www_tag_add_attrib(rss_tag, "version", "2.0");
	www_tag_add_child(page, rss_tag);
	channel_tag = www_tag_new("channel", NULL);
	www_tag_add_child(rss_tag, channel_tag);

	channel_sub_tag = www_tag_new("title", NULL);
	www_tag_add_child(channel_tag, channel_sub_tag);
	str_a = EMPTY_STRALLOC;
	stralloc_cats(&str_a, conf.bbs_name);
	stralloc_cats(&str_a, " System Blog");
	stralloc_0(&str_a);
	channel_sub_tag_content = www_tag_new(NULL, str_a.s);
	free(str_a.s);
	www_tag_add_child(channel_sub_tag, channel_sub_tag_content);

	channel_sub_tag = www_tag_new("link", NULL);
	www_tag_add_child(channel_tag, channel_sub_tag);
	str_a = EMPTY_STRALLOC;
	stralloc_cats(&str_a, www_get_my_url(connection));
	stralloc_cats(&str_a, "blog/");
	stralloc_0(&str_a);
	channel_sub_tag_content = www_tag_new(NULL, str_a.s);
	free(str_a.s);
	www_tag_add_child(channel_sub_tag, channel_sub_tag_content);

	channel_sub_tag = www_tag_new("description", NULL);
	www_tag_add_child(channel_tag, channel_sub_tag);
	str_a = EMPTY_STRALLOC;
	stralloc_cats(&str_a, "News from ");
	stralloc_cats(&str_a, conf.bbs_name);
	stralloc_0(&str_a);
	channel_sub_tag_content = www_tag_new(NULL, str_a.s);
	free(str_a.s);
	www_tag_add_child(channel_sub_tag, channel_sub_tag_content);

	for (size_t i = 0; i < blog_count; i++) {
		struct www_tag *item_tag;
		struct www_tag *title_tag;
		struct www_tag *title_content;
		struct www_tag *description_tag;
		struct www_tag *description_content;
		struct www_tag *pubdate_tag;
		struct www_tag *pubdate_content;
		struct tm entry_time;
        char *title = blog_get_title(i);
        char *entry = blog_get_entry(i);
        time_t edate = blog_get_date(i);
		char datebuf[30];

		item_tag = www_tag_new("item", NULL);
		www_tag_add_child(channel_tag, item_tag);

		title_tag = www_tag_new("title", NULL);
		www_tag_add_child(item_tag, title_tag);

		title_content = www_tag_new(NULL, title);
		www_tag_add_child(title_tag, title_content);

		description_tag = www_tag_new("description", NULL);
		www_tag_add_child(item_tag, description_tag);

		stralloc blog_body = EMPTY_STRALLOC;
   
        ret = md_html(entry, strlen(entry), www_blog_process_output, (void *)&blog_body, 0, 0);

        if (ret != 0) {
            stralloc_cats(&blog_body, "<p>Failed to Parse Markdown</p>");
        }
            
        stralloc_0(&blog_body);

		description_content = www_tag_htmlblock(blog_body.s);
		free(blog_body.s);
		www_tag_add_child(description_tag, description_content);

		gmtime_r(&edate, &entry_time);
		strftime(datebuf, sizeof datebuf, "%a, %d %b %Y %H:%M:%S GMT", &entry_time);

		pubdate_tag = www_tag_new("pubDate", NULL);
		www_tag_add_child(item_tag, pubdate_tag);

		pubdate_content = www_tag_new(NULL, datebuf);
		www_tag_add_child(pubdate_tag, pubdate_content);
        free(title);
        free(entry);
	}

	return www_tag_unwravel(page);
}

char *www_blog(struct MHD_Connection *connection, int offset) {
	struct www_tag *page;
	struct www_tag *cur_tag;
	struct www_tag *child_tag;
	struct www_tag *child_child_tag;
	struct www_tag *child_child_child_tag;
    int blog_count = blog_get_entry_count();
    
    int ret;
    
	page = www_tag_new(NULL, "");
	cur_tag = www_tag_new("div", NULL);
	www_tag_add_attrib(cur_tag, "class", "content-header");
	child_tag = www_tag_new("h2", NULL);
	www_tag_add_child(cur_tag, child_tag);
	www_tag_add_child(child_tag, www_tag_new(NULL, "System Blog"));
	www_tag_add_child(page, cur_tag);

	if (blog_count == 0) {
		cur_tag = www_tag_new("p", NULL);
		www_tag_add_child(cur_tag, www_tag_new(NULL, "No Entries"));
		www_tag_add_child(page, cur_tag);

		return www_tag_unwravel(page);
	}
	for (size_t i = offset; i < blog_count && i < offset + 10; i++) {
		struct tm entry_time;
		int hour;
		char timebuf[16];
		char datebuf[24];
        char *title = blog_get_title(i);
        char *author = blog_get_author(i);
        char *entry = blog_get_entry(i);
        time_t edate = blog_get_date(i);
        
		localtime_r(&edate, &entry_time);
		hour = entry_time.tm_hour;
		strftime(timebuf, sizeof timebuf, "%l:%M", &entry_time);
		strftime(datebuf, sizeof datebuf, " %a, %e %b %Y", &entry_time);

		cur_tag = www_tag_new("div", NULL);
		www_tag_add_attrib(cur_tag, "class", "blog-header");
		www_tag_add_child(page, cur_tag);

		child_tag = www_tag_new("div", NULL);
		www_tag_add_attrib(child_tag, "class", "blog-title");
		www_tag_add_child(cur_tag, child_tag);

		child_child_tag = www_tag_new("h3", NULL);
		www_tag_add_child(child_tag, child_child_tag);

		child_child_child_tag = www_tag_new(NULL, title);
		www_tag_add_child(child_child_tag, child_child_child_tag);

		child_tag = www_tag_new("div", NULL);
		www_tag_add_attrib(child_tag, "class", "blog-date");
		www_tag_add_child(cur_tag, child_tag);

		child_child_tag = www_tag_new(NULL, timebuf);
		www_tag_add_child(child_tag, child_child_tag);

		child_child_tag = www_tag_new(NULL, hour >= 12 ? "pm" : "am");
		www_tag_add_child(child_tag, child_child_tag);

		child_child_tag = www_tag_new(NULL, datebuf);
		www_tag_add_child(child_tag, child_child_tag);

		child_tag = www_tag_new("div", NULL);
		www_tag_add_attrib(child_tag, "class", "blog-author");
		www_tag_add_child(cur_tag, child_tag);

		child_child_tag = www_tag_new(NULL, "by ");
		www_tag_add_child(child_tag, child_child_tag);

		child_child_tag = www_tag_new(NULL, author);
		www_tag_add_child(child_tag, child_child_tag);

		cur_tag = www_tag_new("div", NULL);
		www_tag_add_attrib(cur_tag, "class", "blog-entry");
		www_tag_add_child(page, cur_tag);

		stralloc blog_body = EMPTY_STRALLOC;

        ret = md_html(entry, strlen(entry), www_blog_process_output, (void *)&blog_body, 0, 0);

        if (ret != 0) {
            stralloc_cats(&blog_body, "<p>Failed to Parse Markdown</p>");
        }        
        stralloc_0(&blog_body);
		child_tag = www_tag_htmlblock(blog_body.s);
        free(blog_body.s);
		www_tag_add_child(cur_tag, child_tag);
        free(entry);
        free(title);
        free(author);
	}
    
    cur_tag = www_tag_new("div", NULL);
        
    www_tag_add_attrib(cur_tag, "class", "blog-buttons");
        
    www_tag_add_child(page, cur_tag);
        
	stralloc tempstr = EMPTY_STRALLOC;
	if (offset > 0) {
        child_tag = www_tag_new("div", NULL);
        
        www_tag_add_attrib(child_tag, "class", "blog-prev-btn");
        
        www_tag_add_child(cur_tag, child_tag);
        
        child_child_tag = www_tag_new("a", NULL);
        
        stralloc_cats(&tempstr, www_get_my_url(connection));
        stralloc_cats(&tempstr, "blog/");
        
        if (offset - 10 > 0) {
            stralloc_cat_ulong(&tempstr, offset - 10);
            stralloc_append1(&tempstr, '/');
        }
        stralloc_0(&tempstr);
        
        www_tag_add_attrib(child_child_tag, "href", tempstr.s);
        free(tempstr.s);
        
        www_tag_add_child(child_tag, child_child_tag);
        
        child_child_child_tag = www_tag_new(NULL, "Newer Entries");
        www_tag_add_child(child_child_tag, child_child_child_tag);
        
        tempstr = EMPTY_STRALLOC;
    }

    if (blog_count > offset + 10) {
        child_tag = www_tag_new("div", NULL);
        
        www_tag_add_attrib(child_tag, "class", "blog-next-btn");
        
        www_tag_add_child(cur_tag, child_tag);
        
        child_child_tag = www_tag_new("a", NULL);
        
        stralloc_cats(&tempstr, www_get_my_url(connection));
        stralloc_cats(&tempstr, "blog/");
        
        stralloc_cat_ulong(&tempstr, offset + 10);
        stralloc_append1(&tempstr, '/');

        stralloc_0(&tempstr);
        
        www_tag_add_attrib(child_child_tag, "href", tempstr.s);
        free(tempstr.s);
        
        www_tag_add_child(child_tag, child_child_tag);
        
        child_child_child_tag = www_tag_new(NULL, "Older Entries");
        www_tag_add_child(child_child_tag, child_child_child_tag);
        
        tempstr = EMPTY_STRALLOC;  
    }
    
    child_tag = www_tag_new("div", NULL);
        
    www_tag_add_attrib(child_tag, "class", "blog-rss-btn");
        
    www_tag_add_child(cur_tag, child_tag);
    
	child_child_tag = www_tag_new("a", NULL);

	stralloc rsslink = EMPTY_STRALLOC;

	stralloc_cats(&rsslink, www_get_my_url(connection));
	stralloc_cats(&rsslink, "blog/rss/");
	stralloc_0(&rsslink);

	www_tag_add_attrib(child_child_tag, "href", rsslink.s);
	free(rsslink.s);

	www_tag_add_child(child_tag, child_child_tag);

	child_child_child_tag = www_tag_new(NULL, "RSS Feed");
	www_tag_add_child(child_child_tag, child_child_child_tag);

	return www_tag_unwravel(page);
}

#endif
